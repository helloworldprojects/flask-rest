from flask import Flask
from flask_swagger_ui import get_swaggerui_blueprint

from .resources import map


def create_app(config):
    app = Flask(__name__)

    swagger_blueprint = get_swaggerui_blueprint(
        base_url='/ui',
        api_url='/static/openapi.yml',
        config={
            'app_name': 'MVS Swagger UI %s' % config['ENVIRONMENT']
        }
    )
    app.register_blueprint()
    app.register_blueprint(map.blueprint, url_prefix='/map')

    return app
