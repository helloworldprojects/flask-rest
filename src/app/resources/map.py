from flask import Blueprint
from flask_restful import Api, Resource, reqparse

blueprint = Blueprint("map-endpoints", __name__)
api = Api(blueprint)


class Crossroads(Resource):
    def __init__(self):
        self.reqparser = reqparse.RequestParser()
        self.reqparser.add_argument(
            "area_id",
            type=int,
            help="Area id",
            required=True
        )
        super(Crossroads, self).__init__()

    def get(self):
        args = self.reqparser.parse_args()
        return {"data": args}


api.add_resource(Crossroads, "/hello")
